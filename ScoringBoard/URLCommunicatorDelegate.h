//
//  URLCommunicatorDelegate.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol URLCommunicatorDelegate
- (void)receivePlayersJSON:(NSData *)objectNotation;
- (void)fetchingPlayersFailedWithError:(NSError *)error;
@end
