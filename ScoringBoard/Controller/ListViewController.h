//
//  ListViewController.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/2/10.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@end
