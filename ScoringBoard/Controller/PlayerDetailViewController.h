//
//  PlayerDetailViewController.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/3.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Player.h"

@interface PlayerDetailViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate>
@property(nonatomic) Player *play;

- (void) configueview;
@end
