//
//  PlayerDetailViewController.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/3.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "PlayerDetailViewController.h"
#import "PlayerView.h"
#import "VideoCell.h"
#import <AVFoundation/AVFoundation.h>

@interface PlayerDetailViewController (){
    //BOOL _isPlaying;
    //CGFloat _totalSec;
    //NSString *_totalTime;
    //NSDateFormatter *_dateFormatter;
    
    BOOL _videoNeedPlay;
    BOOL _videoPlaying;
    BOOL _didLayoutSubview;
}
@property(nonatomic,weak) IBOutlet UIImageView *avatarImage;
@property(nonatomic,weak) IBOutlet UILabel *posLbl;
@property(nonatomic,weak) IBOutlet UILabel *teamLbl;
@property(nonatomic,weak) IBOutlet UILabel *birthLbl;
@property(nonatomic,weak) IBOutlet UILabel *weightLbl;
@property(nonatomic,weak) IBOutlet UILabel *heightLbl;
@property(nonatomic,weak) IBOutlet UILabel *numberLbl;
#pragma mark - custom AVPlayer
@property(nonatomic,weak) IBOutlet PlayerView *playerView;
@property(nonatomic,weak) IBOutlet UIButton *playPauseBtn;
@property(nonatomic,weak) IBOutlet UISlider *slider;
@property(nonatomic,weak) IBOutlet UILabel *timeLabel;
@property(nonatomic,weak) IBOutlet UIProgressView *prgs;
@property(nonatomic) AVPlayer *player;
@property(nonatomic) AVPlayerItem *playerItem;
@property(nonatomic) id playbackTimeObserver;
#pragma mark - Embedded Video in Table Cell
@property(nonatomic,weak)IBOutlet UITableView *videoTable;
@property(nonatomic) NSMutableArray *videoUrl;
@property(nonatomic) NSIndexPath *indexplay;
@property(nonatomic) NSIndexPath *oldIndexplay;
@property(nonatomic,weak)IBOutlet UIView *playerInfoView;

- (IBAction)closeView;
- (IBAction)playPauseBtnTouched:(id)sender;
- (IBAction)slidervaluechanged:(id)sender;
@end

@implementation PlayerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //_isPlaying = NO;
    _videoNeedPlay = NO;
    _videoPlaying = NO;
    _didLayoutSubview = NO;
    //[self.playPauseBtn setTitle:@"Play" forState:UIControlStateNormal];
    [self configueview];
    //[self initplayer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.videoTable setContentInset:UIEdgeInsetsMake(self.playerInfoView.bounds.size.height, 0.f, 0.f, 0.f)];
    [self.videoTable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    _didLayoutSubview = YES;
    //float headerImageYOffset = 88 + self.containerUIView.bounds.size.height - self.view.bounds.size.height;
    //CGRect headerImageFrame = _containerUIView.frame;
    //headerImageFrame.origin.y = headerImageYOffset;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) configueview{    
    _avatarImage.image = _play.avatarimg;
    _posLbl.text = _play.position;
    _teamLbl.text = _play.team;
    _birthLbl.text = _play.birth;
    _weightLbl.text = _play.weight;
    _heightLbl.text = _play.height;
    _numberLbl.text = _play.number;
    
    _videoUrl = [NSMutableArray arrayWithCapacity:5];
    _videoUrl = [NSMutableArray arrayWithObjects:@"https://nmdl.udn.com/vods3/nba20170315160006019/media_b2628000_0.ts", @"https://nmdl.udn.com/vods3/nba20170315145106830/media_b878000_0.ts", @"https://nmdl.udn.com/vods3/nba20170315144806171/media_b878000_0.ts",@"https://nmdl.udn.com/vods3/nba20170315144207169/media_b878000_0.ts",@"https://nmdl.udn.com/vods3/nba20170315143905760/chunklist_b878000.m3u8",nil];
}
/*
- (void)initplayer{
    NSURL *url = [NSURL URLWithString:@"https://r1---sn-ipoxu-un56.gvt1.com/videoplayback/id/73b4e4c82b28955e/itag/18/source/gfp_video_ads/requiressl/yes/acao/yes/mime/video%2Fmp4/ip/223.140.76.249/ipbits/0/expire/1489348592/sparams/acao,expire,id,ip,ipbits,itag,mime,mm,mn,ms,mv,pl,requiressl,source/signature/013D02EC53A3B25ACF8F8E42688DE26D6CD9B4A7.60CBC918617127D4C4A14B44478A3257AF654DF9/key/cms1/cms_redirect/yes/mm/28/mn/sn-ipoxu-un56/ms/nvh/mt/1489326906/mv/m/pl/17/file/file.mp4"];
    self.playerItem = [AVPlayerItem playerItemWithURL:url];
    [self.playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    [self.playerView setPlayer:self.player];
    CGFloat durationSec = CMTimeGetSeconds(self.playerItem.asset.duration);
    self.timeLabel.text = [self convertTime:durationSec];
}

#pragma mark - KVO & Notificaton
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    AVPlayerItem *playerItm = (AVPlayerItem *)object;
    if([keyPath isEqualToString:@"status"]){
        if([playerItm status] == AVPlayerStatusReadyToPlay){
            NSLog(@"AVPlayerStatusReadyToPlay");
            self.playPauseBtn.hidden = NO;
            self.slider.hidden = NO;
            self.prgs.hidden = NO;
            CMTime duration = self.playerItem.asset.duration;
            _totalSec = playerItm.asset.duration.value/playerItm.asset.duration.timescale;
            _totalTime = [self convertTime:_totalSec];
            [self customVideoSlider:duration];
            NSLog(@"movie total duration:%f",CMTimeGetSeconds(duration));
            [self monitoringPlayback:self.playerItem];
        }else if([playerItm status] == AVPlayerStatusFailed){
            NSLog(@"AVPlayerStatusFailed");
        }
    }else if([keyPath isEqualToString:@"loadedTimeRanges"]){
        NSTimeInterval timeInterval = [self avaliableDuration];
        NSLog(@"Time Interval:%f",timeInterval);
        CMTime duration = self.playerItem.asset.duration;
        _totalSec = CMTimeGetSeconds(duration);
        [self.prgs setProgress:timeInterval/_totalSec animated:YES];
    }
}

- (void)moviePlayDidEnd:(NSNotification *)notification {
    NSLog(@"Play end");
    
    _isPlaying = NO;
    __weak typeof(self) weakSelf = self;
    [self.playerView.player seekToTime:kCMTimeZero completionHandler:^(BOOL finished) {
        [weakSelf.slider setValue:0.0 animated:YES];
        [weakSelf.playPauseBtn setTitle:@"Play" forState:UIControlStateNormal];
    }];
    //[self.playerView removeFromSuperview];
}
*/
#pragma mark - Action
- (IBAction)closeView {
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
- (IBAction)playPauseBtnTouched:(id)sender{
    if(_isPlaying){
        [self.player pause];
        [self.playPauseBtn setTitle:@"play" forState:UIControlStateNormal];
    }else{
        [self.player play];
        [self.playPauseBtn setTitle:@"Pause" forState:UIControlStateNormal];
    }
    _isPlaying = !_isPlaying;
}

- (IBAction)slidervaluechanged:(UISlider *)sender{
    UISlider *slider = (UISlider *)sender;
    NSLog(@"value end:%f",slider.value);
    CMTime changedTime = CMTimeMakeWithSeconds(slider.value, 1);
    
    __weak typeof(self) weakSelf = self;
    [self.playerView.player seekToTime:changedTime completionHandler:^(BOOL finished) {
        [weakSelf.playerView.player play];
        [weakSelf.playPauseBtn setTitle:@"Stop" forState:UIControlStateNormal];
        CGFloat currentTime = CMTimeGetSeconds([self.player currentTime]);
        self.timeLabel.text = [self convertTime:currentTime];
    }];
}

#pragma mark - UI update
- (void)monitoringPlayback:(AVPlayerItem *)playerItem{
    __weak typeof(self) weakself = self;
    self.playbackTimeObserver = [self.playerView.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:NULL usingBlock:^(CMTime time){
        CGFloat currentSec = playerItem.currentTime.value/playerItem.currentTime.timescale;
        [weakself.slider setValue:currentSec animated:YES];
        CGFloat remainSec = _totalSec - currentSec;
        NSString *remainSecTimeStr = [self convertTime:remainSec];
        
        weakself.timeLabel.text = [NSString stringWithFormat:@"%@",remainSecTimeStr];
    }];
}

- (NSTimeInterval)avaliableDuration{
    NSArray *loadedTimeRanges = [[self.playerView.player currentItem] loadedTimeRanges];
    CMTimeRange timeRange = [loadedTimeRanges.firstObject CMTimeRangeValue];
    float startSec = CMTimeGetSeconds(timeRange.start);
    float durationSec = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result = startSec+durationSec;
    return result;
}

- (void)customVideoSlider:(CMTime)duration{
    self.slider.maximumValue = CMTimeGetSeconds(duration);
    UIGraphicsBeginImageContextWithOptions((CGSize){.5,.5}, NO, 0.0f);
    UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.slider setMaximumTrackImage:transparentImage forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:transparentImage forState:UIControlStateNormal];
}

- (NSString *)convertTime:(CGFloat)totalSec{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:totalSec];
    if((totalSec/3600)>1){
        [[self dateFormatter] setDateFormat:@"HH:mm:ss"];
    }else{
        [[self dateFormatter] setDateFormat:@"mm:ss"];
    }
    NSString *convertTimeStr = [[self dateFormatter] stringFromDate:date];
    return convertTimeStr;
}

- (NSDateFormatter *)dateFormatter{
    if(!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    return _dateFormatter;
}

- (void)dealloc {
    [self.playerItem removeObserver:self forKeyPath:@"status" context:nil];
    [self.playerItem removeObserver:self forKeyPath:@"loadedTimeRanges" context:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
*/
#pragma mark - Embeede Video in tableview
- (void)scrollViewDidScroll:(UIScrollView *)ascrollView{
    
    CGFloat scrollOffset = -ascrollView.contentOffset.y;
    CGFloat yPos = scrollOffset -_playerInfoView.bounds.size.height;
    _playerInfoView.frame = CGRectMake(0, yPos+64, _playerInfoView.frame.size.width, _playerInfoView.frame.size.height);
    float alpha=1.0-(-yPos/ _playerInfoView.frame.size.height);
    _playerInfoView.alpha=alpha;
    //float fontSize=24-(-yPos/20);
    //_mUILabel.font=[UIFont systemFontOfSize:fontSize];
    
    if(_didLayoutSubview == YES){
    NSArray* cells = _videoTable.visibleCells;
    for(VideoCell *cell in cells)
    {
        if (((cell.frame.origin.y >= ascrollView.contentOffset.y) &&
            ((cell.frame.origin.y + cell.frame.size.height)<(ascrollView.contentOffset.y + ascrollView.bounds.size.height))) && (self.oldIndexplay != [_videoTable indexPathForCell:cell]))
        {
            _videoNeedPlay = YES;
            self.indexplay = [_videoTable indexPathForCell:cell];
            NSLog(@"\nfullVisible:%@, %@",self.indexplay, self.oldIndexplay);
            dispatch_async(dispatch_get_main_queue(), ^{
                [_videoTable reloadData];
            });
            break;
        }
    }}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] init];
    headView.backgroundColor = [UIColor grayColor];
    return headView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *headView = [[UIView alloc] init];
    headView.backgroundColor = [UIColor grayColor];
    return headView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VideoCell class])];
    if (_videoNeedPlay){
        
        if(_videoPlaying){
            if(self.oldIndexplay == indexPath){
                NSLog(@"\nPause:%@, %@",self.indexplay, self.oldIndexplay);
                [cell.player pause];
                _videoPlaying = NO;
            }
        }
        
        if(!_videoPlaying && self.indexplay == indexPath){
            self.oldIndexplay = indexPath;
            NSURL *url = [NSURL URLWithString:_videoUrl[indexPath.section]];
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
            dispatch_async(queue, ^{
            [cell setupCellByUrl:url];
            //cell.playerItem = [AVPlayerItem playerItemWithURL:url];
            //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tblMoviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:cell.playerItem];
            //cell.player = [AVPlayer playerWithPlayerItem:cell.playerItem];
            dispatch_sync(dispatch_get_main_queue(), ^{
            //cell.playerLayer = [AVPlayerLayer playerLayerWithPlayer:cell.player];
            //cell.playerLayer.frame = cell.playerView.bounds;
            //cell.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            //[cell.playerView.layer addSublayer:cell.playerLayer];
            [cell.player play];
            [cell.player setVolume:1.0];
            _videoPlaying = YES;
            _videoNeedPlay = NO;
            NSLog(@"\nPlay:%@, %@",self.indexplay, self.oldIndexplay);
            });
            });
        }
    }
    return cell;
}

/*- (void)tblMoviePlayDidEnd:(NSNotification *)notification{
    _videoPlaying = NO;
    _videoNeedPlay = NO;
}*/
@end
