//
//  ListViewController.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/2/10.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "ListViewController.h"
#import "PlayerCell.h"
#import "Player.h"
#import "PlayerDetailViewController.h"
#import "URLManager.h"
#import "URLCommunicator.h"

@interface ListViewController() <URLManagerDelegate>

@property (nonatomic, weak) IBOutlet UITableView *mainTable;
@property (nonatomic) NSArray *players;
@property (nonatomic) URLManager *manager;
@property (nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@end

@implementation ListViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    _manager = [[URLManager alloc] init];
    _manager.communicator = [[URLCommunicator alloc] init];
    _manager.communicator.delegate = _manager;
    _manager.delegate = self;
    
    [self startFetchingPlayers];
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.center = CGPointMake(self.mainTable.frame.size.width * 0.5, self.mainTable.frame.size.height * 0.5);
    [self.mainTable addSubview:_spinner];
    [_spinner startAnimating];
}

#pragma mark - Action
- (IBAction)closeView {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _players.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlayerCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PlayerCell class])];
    [cell setupCellByPlayer:_players[indexPath.row]];
    return cell;
}

#pragma Notification Observer
- (void) startFetchingPlayers{
    [_manager fetchPlayers];
}

#pragma URLManagerDelegate
- (void) didReceivePlayers:(NSArray *)player{
    _players = player;
    dispatch_async(dispatch_get_main_queue(), ^{
    [_spinner stopAnimating];
    [self.mainTable reloadData];
    });
}

- (void) fetchingPlayersFailedWithError:(NSError *)error{
    NSLog(@"\nError");
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSIndexPath *path = [self.mainTable indexPathForSelectedRow];
    
    UINavigationController *nav = segue.destinationViewController;
    PlayerDetailViewController *pDVC = (PlayerDetailViewController *)nav.topViewController;
    pDVC.play = _players[path.row];
    
}
@end
