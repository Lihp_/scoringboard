//
//  ScoringBoardViewController.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/1/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoringBoardViewController : UIViewController

@property(weak,nonatomic) IBOutlet UILabel *scoringBoardA;
@property(weak,nonatomic) IBOutlet UILabel *scoringBoardB;
@property(weak,nonatomic) IBOutlet UILabel *displayCount;
@property(weak,nonatomic) IBOutlet UILabel *homeLable;
@property(weak,nonatomic) IBOutlet UILabel *awayLable;
@property(weak,nonatomic) IBOutlet UIButton *homePlusBtn;
@property(weak,nonatomic) IBOutlet UIButton *homeMinusBtn;
@property(weak,nonatomic) IBOutlet UIButton *awayPlusBtn;
@property(weak,nonatomic) IBOutlet UIButton *awayMinusBtn;


-(IBAction)plusPointA;
-(IBAction)minusPointA;
-(IBAction)plusPointB;
-(IBAction)minusPointB;
-(IBAction)Start;

-(void)MinusSec:(NSTimer *)sender;

@end

