//
//  Player.m
//  ScoringBoard
//
//  Created by JLee21 on 2017/2/27.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "Player.h"

@implementation Player
+ (NSArray *)playersFromJSON:(NSData *)objectNotation error:(NSError **)error{
    NSError *localError = nil;
    
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:objectNotation options:0 error:&localError];
    
    if(localError != nil){
        *error = localError;
        return nil;
    }
    
    NSMutableArray *players = [[NSMutableArray alloc]init];
    NSDictionary *payload = [parsedObject valueForKey:@"payload"];
    NSArray *playerlist = [payload valueForKey:@"players"];
    
    for(NSArray *playerelement in playerlist){
        
        Player *player = [[Player alloc] init];
        NSDictionary *playerProfile = [playerelement valueForKey:@"playerProfile"];
        NSDictionary *teamProfile = [playerelement valueForKey:@"teamProfile"];
        NSDictionary *statAverage = [playerelement valueForKey:@"statAverage"];
        
        player.avatarUrlId = playerProfile[@"playerId"];
        NSString *imgurlstr = [NSString stringWithFormat:@"https://ak-static.cms.nba.com/wp-content/uploads/headshots/nba/latest/260x190/%@.png",player.avatarUrlId];
        NSData *imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imgurlstr]];
        player.avatarimg = [UIImage imageWithData: imageData];
        
        player.name = playerProfile[@"displayName"];
        player.position = playerProfile[@"position"];
        player.number = playerProfile[@"jerseyNo"];
        player.weight = playerProfile[@"weight"];
        player.height = playerProfile[@"height"];
        player.team = teamProfile[@"nameEn"];
        player.pointsPg = [statAverage[@"pointsPg"] floatValue];
        player.rebsPg = [statAverage[@"rebsPg"] floatValue];
        player.assistsPg = [statAverage[@"assistsPg"] floatValue];
        player.stealsPg = [statAverage[@"stealsPg"] floatValue];
        player.turnoversPg = [statAverage[@"turnoversPg"] floatValue];
        
        [players addObject:player];
    }
    
    return players;
}
@end
