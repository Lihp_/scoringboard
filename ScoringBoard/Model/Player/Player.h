//
//  Player.h
//  ScoringBoard
//
//  Created by JLee21 on 2017/2/27.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Player : NSObject
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *avatarUrlId;
@property (nonatomic) UIImage *avatarimg;
@property (nonatomic) float pointsPg;
@property (nonatomic) float rebsPg;
@property (nonatomic) float assistsPg;
@property (nonatomic) float stealsPg;
@property (nonatomic) float turnoversPg;
@property (nonatomic) NSString *position;
@property (nonatomic) NSString *birth;
@property (nonatomic) NSString *height;
@property (nonatomic) NSString *weight;
@property (nonatomic) NSString *team;
@property (nonatomic) NSString *number;

+ (NSArray *)playersFromJSON:(NSData *)objectNotation error:(NSError **)error;
@end
