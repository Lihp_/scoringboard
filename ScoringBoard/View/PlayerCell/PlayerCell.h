//
//  PlayerCell.h
//  ScoringBoard
//
//  Created by JLee21 on 2017/2/25.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Player;
@interface PlayerCell : UITableViewCell
- (void)setupCellByPlayer:(Player *)player;
@end
