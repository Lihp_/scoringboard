//
//  PlayerCell.m
//  ScoringBoard
//
//  Created by JLee21 on 2017/2/25.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "PlayerCell.h"
#import "Player.h"

@interface PlayerCell()
@property (nonatomic, weak) IBOutlet UIImageView *avatarImg;
@property (nonatomic, weak) IBOutlet UILabel *nameLbl;
@property (nonatomic, weak) IBOutlet UILabel *ptsLbl;
@property (nonatomic, weak) IBOutlet UILabel *rebLbl;
@property (nonatomic, weak) IBOutlet UILabel *astLbl;
@property (nonatomic, weak) IBOutlet UILabel *stlLbl;
@property (nonatomic, weak) IBOutlet UILabel *tovLbl;
@end

@implementation PlayerCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setupCellByPlayer:(Player *)player {
    _avatarImg.image = player.avatarimg;
    _nameLbl.text = player.name;
    _ptsLbl.text = [NSString stringWithFormat:@"%.1f",player.pointsPg];
    _rebLbl.text = [NSString stringWithFormat:@"%.1f",player.rebsPg];
    _astLbl.text = [NSString stringWithFormat:@"%.1f",player.assistsPg];
    _stlLbl.text = [NSString stringWithFormat:@"%.1f",player.stealsPg];
    _tovLbl.text = [NSString stringWithFormat:@"%.1f",player.turnoversPg];
}

@end
