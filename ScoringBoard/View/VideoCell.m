//
//  VideoCell.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/13.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "VideoCell.h"
#import <AVFoundation/AVFoundation.h>

@interface VideoCell (){    
    BOOL _isPlaying;
    CGFloat _totalSec;
    NSString *_totalTime;
    NSDateFormatter *_dateFormatter;
}
@end

@implementation VideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupCellByUrl:(NSURL *)url{
    
    //self.playerItem = [AVPlayerItem playerItemWithURL:url];
    AVURLAsset *urlAsset = [AVURLAsset assetWithURL:url];
    self.playerItem = [AVPlayerItem playerItemWithAsset:urlAsset];
    [self.playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    [self.playerView setPlayer:self.player];
    CGFloat durationSec = CMTimeGetSeconds(self.playerItem.asset.duration);
    NSLog(@"durationSec:%lld",self.playerItem.asset.duration.value);
    self.timeLabel.text = [self convertTime:durationSec];
}

/*- (void)initplayer{
    NSURL *url = [NSURL URLWithString:@"https://r1---sn-ipoxu-un56.gvt1.com/videoplayback/id/73b4e4c82b28955e/itag/18/source/gfp_video_ads/requiressl/yes/acao/yes/mime/video%2Fmp4/ip/223.140.76.249/ipbits/0/expire/1489348592/sparams/acao,expire,id,ip,ipbits,itag,mime,mm,mn,ms,mv,pl,requiressl,source/signature/013D02EC53A3B25ACF8F8E42688DE26D6CD9B4A7.60CBC918617127D4C4A14B44478A3257AF654DF9/key/cms1/cms_redirect/yes/mm/28/mn/sn-ipoxu-un56/ms/nvh/mt/1489326906/mv/m/pl/17/file/file.mp4"];
    self.playerItem = [AVPlayerItem playerItemWithURL:url];
    [self.playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.playerItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayDidEnd:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.playerItem];
    self.player = [AVPlayer playerWithPlayerItem:self.playerItem];
    [self.playerView setPlayer:self.player];
    CGFloat durationSec = CMTimeGetSeconds(self.playerItem.asset.duration);
    self.timeLabel.text = [self convertTime:durationSec];
}*/

#pragma mark - KVO & Notificaton
- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    AVPlayerItem *playerItm = (AVPlayerItem *)object;
    if([keyPath isEqualToString:@"status"]){
        if([playerItm status] == AVPlayerStatusReadyToPlay){
            NSLog(@"AVPlayerStatusReadyToPlay");
            self.playPauseBtn.hidden = NO;
            self.slider.hidden = NO;
            self.prgs.hidden = NO;
            self.timeLabel.hidden = NO;
            CMTime duration = self.playerItem.asset.duration;
            NSLog(@"%lld, %d, %lld, %d",duration.value,duration.timescale,playerItm.asset.duration.value,playerItm.asset.duration.timescale);
            _totalSec = playerItm.asset.duration.value/playerItm.asset.duration.timescale;
            _totalTime = [self convertTime:_totalSec];
            [self customVideoSlider:duration];
            NSLog(@"movie total duration:%f",CMTimeGetSeconds(duration));
            [self monitoringPlayback:self.playerItem];
        }else if([playerItm status] == AVPlayerStatusFailed){
            NSLog(@"AVPlayerStatusFailed");
        }
    }else if([keyPath isEqualToString:@"loadedTimeRanges"]){
        NSTimeInterval timeInterval = [self avaliableDuration];
        NSLog(@"Time Interval:%f",timeInterval);
        CMTime duration = self.playerItem.asset.duration;
        _totalSec = CMTimeGetSeconds(duration);
        [self.prgs setProgress:timeInterval/_totalSec animated:YES];
    }
}

- (void)moviePlayDidEnd:(NSNotification *)notification {
    NSLog(@"Play end");
    
    _isPlaying = NO;
    __weak typeof(self) weakSelf = self;
    [self.playerView.player seekToTime:kCMTimeZero completionHandler:^(BOOL finished) {
        [weakSelf.slider setValue:0.0 animated:YES];
        [weakSelf.playPauseBtn setTitle:@"Play" forState:UIControlStateNormal];
    }];
    //[self.playerView removeFromSuperview];
}

- (IBAction)playPauseBtnTouched:(id)sender{
    if(_isPlaying){
        [self.player pause];
        [self.playPauseBtn setTitle:@"play" forState:UIControlStateNormal];
    }else{
        [self.player play];
        [self.playPauseBtn setTitle:@"Pause" forState:UIControlStateNormal];
    }
    _isPlaying = !_isPlaying;
}

- (IBAction)slidervaluechanged:(UISlider *)sender{
    UISlider *slider = (UISlider *)sender;
    NSLog(@"value end:%f",slider.value);
    CMTime changedTime = CMTimeMakeWithSeconds(slider.value, 1);
    
    __weak typeof(self) weakSelf = self;
    [self.playerView.player seekToTime:changedTime completionHandler:^(BOOL finished) {
        [weakSelf.playerView.player play];
        [weakSelf.playPauseBtn setTitle:@"Stop" forState:UIControlStateNormal];
        CGFloat currentTime = CMTimeGetSeconds([self.player currentTime]);
        self.timeLabel.text = [self convertTime:currentTime];
    }];
}

#pragma mark - UI update
- (void)monitoringPlayback:(AVPlayerItem *)playerItem{
    __weak typeof(self) weakself = self;
    self.playbackTimeObserver = [self.playerView.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:NULL usingBlock:^(CMTime time){
        CGFloat currentSec = playerItem.currentTime.value/playerItem.currentTime.timescale;
        [weakself.slider setValue:currentSec animated:YES];
        CGFloat remainSec = _totalSec - currentSec;
        NSString *remainSecTimeStr = [self convertTime:remainSec];
        
        weakself.timeLabel.text = [NSString stringWithFormat:@"%@",remainSecTimeStr];
    }];
}

- (NSTimeInterval)avaliableDuration{
    NSArray *loadedTimeRanges = [[self.playerView.player currentItem] loadedTimeRanges];
    CMTimeRange timeRange = [loadedTimeRanges.firstObject CMTimeRangeValue];
    float startSec = CMTimeGetSeconds(timeRange.start);
    float durationSec = CMTimeGetSeconds(timeRange.duration);
    NSTimeInterval result = startSec+durationSec;
    return result;
}

- (void)customVideoSlider:(CMTime)duration{
    self.slider.maximumValue = CMTimeGetSeconds(duration);
    UIGraphicsBeginImageContextWithOptions((CGSize){.5,.5}, NO, 0.0f);
    UIImage *transparentImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self.slider setMaximumTrackImage:transparentImage forState:UIControlStateNormal];
    [self.slider setMinimumTrackImage:transparentImage forState:UIControlStateNormal];
}

- (NSString *)convertTime:(CGFloat)totalSec{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:totalSec];
    if((totalSec/3600)>1){
        [[self dateFormatter] setDateFormat:@"HH:mm:ss"];
    }else{
        [[self dateFormatter] setDateFormat:@"mm:ss"];
    }
    NSString *convertTimeStr = [[self dateFormatter] stringFromDate:date];
    return convertTimeStr;
}

- (NSDateFormatter *)dateFormatter{
    if(!_dateFormatter){
        _dateFormatter = [[NSDateFormatter alloc] init];
    }
    return _dateFormatter;
}

- (void)dealloc {
    //[self.playerItem removeObserver:self forKeyPath:@"status" context:nil];
    [self.playerItem removeObserver:self forKeyPath:@"loadedTimeRanges" context:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
