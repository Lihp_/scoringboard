//
//  AVPlayerView.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/10.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class AVPlayer;

@interface PlayerView : UIView

@property(nonatomic) AVPlayer *player;

@end
