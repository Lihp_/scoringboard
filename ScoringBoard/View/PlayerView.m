//
//  AVPlayerView.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/10.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import "PlayerView.h"

@implementation PlayerView

+ (Class)layerClass{
    return [AVPlayerLayer class];
}

- (AVPlayer*)player{
    return [(AVPlayerLayer *)[self layer] player];;
}

- (void)setPlayer:(AVPlayer *)player{
    ((AVPlayerLayer *)[self layer]).videoGravity = AVLayerVideoGravityResizeAspectFill;
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end
