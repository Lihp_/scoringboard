//
//  VideoCell.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/13.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerView.h"

@interface VideoCell : UITableViewCell
@property(nonatomic) AVPlayerItem *playerItem;
@property(nonatomic) AVPlayer *player;
//@property(nonatomic) AVPlayerLayer *playerLayer;
//@property(nonatomic) IBOutlet UIView *playerView;
@property(nonatomic,weak) IBOutlet PlayerView *playerView;
@property(nonatomic,weak) IBOutlet UIButton *playPauseBtn;
@property(nonatomic,weak) IBOutlet UISlider *slider;
@property(nonatomic,weak) IBOutlet UILabel *timeLabel;
@property(nonatomic,weak) IBOutlet UIProgressView *prgs;
@property(nonatomic) id playbackTimeObserver;

- (void)setupCellByUrl:(NSURL *)url;
- (IBAction)playPauseBtnTouched:(id)sender;
- (IBAction)slidervaluechanged:(id)sender;
@end
