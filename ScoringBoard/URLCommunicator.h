//
//  URLCommunicator.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol URLCommunicatorDelegate;

@interface URLCommunicator : NSObject
@property (nonatomic,weak) id<URLCommunicatorDelegate> delegate;

- (void)searchPlayers;
@end
