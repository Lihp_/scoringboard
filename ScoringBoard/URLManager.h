//
//  URLManager.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//
#import "URLCommunicatorDelegate.h"
#import "URLManagerDelegate.h"
#import <Foundation/Foundation.h>

@class URLCommunicator;

@interface URLManager : NSObject <URLCommunicatorDelegate>
@property (nonatomic,strong) URLCommunicator *communicator;
@property (nonatomic,weak) id<URLManagerDelegate> delegate;

- (void)fetchPlayers;
@end
