//
//  URLManager.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//
#import "URLCommunicator.h"
#import "URLManager.h"
#import "Player.h"

@implementation URLManager

- (void) fetchPlayers{
    //Entry for searchPlayers
    [self.communicator searchPlayers];
}

#pragma URLCommunicatorDelegate

- (void)receivePlayersJSON:(NSData *)objectNotation{
    //Raw data -> JSON -> (Player *)
    NSError *error;
    NSArray *players = [Player playersFromJSON:objectNotation error:&error];
    
    if(error != nil){
        [self.delegate fetchingPlayersFailedWithError:error];
    }else{
        [self.delegate didReceivePlayers:players];
    }
}
- (void)fetchingPlayersFailedWithError:(NSError *)error{
    //Error
    [self.delegate fetchingPlayersFailedWithError:error];
}
@end
