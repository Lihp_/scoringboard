//
//  URLCommunicator.m
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//
#import "URLCommunicator.h"
#import "URLCommunicatorDelegate.h"

#import <Foundation/Foundation.h>

@implementation URLCommunicator

- (void)searchPlayers{
    //Use URL parser raw data
    
    NSString *urlString = @"http://tw.global.nba.com/stats2/league/playerstats.json?conference=All&country=All&individual=All&locale=zh_TW&pageIndex=0&position=All&qualified=false&season=2016&seasonType=2&split=All+Team&statType=points&team=All&total=perGame";
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:[[NSURLRequest alloc] initWithURL:url]
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
                   if(error){
                       [self.delegate fetchingPlayersFailedWithError:error];
                   }else{
                       [self.delegate receivePlayersJSON:data];
                   }
               }] resume];
}

@end
