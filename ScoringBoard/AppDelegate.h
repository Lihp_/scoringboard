//
//  AppDelegate.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/1/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

