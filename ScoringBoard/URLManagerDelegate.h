//
//  URLManagerDelegate.h
//  ScoringBoard
//
//  Created by Igounorca on 2017/3/7.
//  Copyright © 2017年 Igounorca. All rights reserved.
//
#import "Player.h"

#import <Foundation/Foundation.h>

@protocol URLManagerDelegate
- (void)didReceivePlayers:(NSArray *)player;
- (void)fetchingPlayersFailedWithError:(NSError *)error;
@end
